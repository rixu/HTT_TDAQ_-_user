/*
 * ./ccache_converter INFILE
 *
 * Converts the ccache format in FTKSim to HTTSim:
 *
 * Trees "LayerN" have branches refactored:
 *      firstPattern -> firstPID
 *      nPattern.numPattern -> nPatterns
 *      nData -> nCodes
 *      data[nData]/I -> codes[nCodes]/b
 */


#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TTreeReaderArray.h"
#include "TString.h"
#include "TRegexp.h"
#include "TKey.h"

#include <iostream>

using namespace std;



void ProcessLayer(TTree * tin, TFile & outfile)
{
    // setup TTree readers
    TTreeReader reader(tin);
    TTreeReaderValue<Int_t> ssid(reader, "ssid");
    TTreeReaderValue<Int_t> sector(reader, "sector");
    TTreeReaderValue<Int_t> firstPattern(reader, "firstPattern");
    TTreeReaderValue<Int_t> nPattern(reader, "nPattern.numPattern");
    TTreeReaderValue<Int_t> nData(reader, "nData");
    TTreeReaderArray<Int_t> data(reader, "data");

    // setup TTree branches in output
    Int_t ssid_, sector_, firstPID_, nPatterns_, nCodes_;
    UChar_t codes[1000];

    outfile.cd();
    TTree *tout = new TTree(tin->GetName(), tin->GetTitle());
    tout->Branch("ssid", &ssid_);
    tout->Branch("sector", &sector_);
    tout->Branch("firstPID", &firstPID_);
    tout->Branch("nPatterns", &nPatterns_);
    tout->Branch("nCodes", &nCodes_);
    tout->Branch("codes", codes, "codes[nCodes]/b");

    while (reader.Next())
    {
        ssid_ = *ssid;
        sector_ = *sector;
        firstPID_ = *firstPattern;
        nPatterns_ = *nPattern;
        nCodes_ = *nData;
        for (int i = 0; i < nCodes_; i++)
            codes[i] = static_cast<UChar_t>(static_cast<UInt_t>(data[i]));

        tout->Fill();
    }
}


int main(int argc, char* argv[])
{
    TFile infile(argv[1], "READ");
    TFile outfile("out.root", "CREATE");
    if (infile.IsZombie() || outfile.IsZombie())
    {
        cout << "Unable to open a file\n";
        return -1;
    }

    // Loop over keys in the file
    for (const auto && obj : *infile.GetListOfKeys())
    {
        TKey *key = dynamic_cast<TKey*>(obj);
        if (!key) throw std::runtime_error("Unable to cast a key\n");

        TString name(key->GetName());
        if (name.BeginsWith("Layer"))
        {
            int layer = TString(name(TRegexp("[0-9]+"))).Atoi();
            if (layer < 0)
            {
                cout << "Found a layer < 0: " << name;
                continue;
            }

            ProcessLayer(static_cast<TTree*>(infile.Get(name)), outfile);
        }
        else // copy the tree over
        {
            // https://root.cern.ch/root/html/tutorials/io/copyFiles.C.html
            TClass *cl = gROOT->GetClass(key->GetClassName());
            if (cl->InheritsFrom(TTree::Class()))
            {
                TTree *T = (TTree*)infile.Get(key->GetName());
                outfile.cd();
                TTree *newT = T->CloneTree(-1,"fast");
                (void)newT;
            }
        }
    } // end loop over keys

    outfile.Write();

    return 0;
}

