/* plot.cxx - Riley Xu -- July 16th 2018
 *
 * Makes a graph with manual entry
 */

#include "TROOT.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TFrame.h"
#include "TLegend.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TH1.h"
#include "TLatex.h"
#include "TMultiGraph.h"
#include <iostream>
#include <vector>

using namespace std;

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "AtlasUtils.C"


vector<double> htt_xall;
vector<double> htt_yall;
vector<double> hough_xall;
vector<double> hough_yall;


void christian_DC_summary()
{
    vector<double> x1 = { // 6o8, 1 WC, fast sim
        0.996560847,
        0.995714286,
        0.994444444,
        0.992222222,
        0.988201058,
        0.980582011
    };
    vector<double> y1 = {
        2307.806691,
        1652.04461 ,
        1139.033457,
        773.2342007,
        518.9591078,
        344.9814126
    };

    vector<double> x2 = { // 6o8, 1 WC, full sim
        0.99952381 ,
        0.999100529,
        0.997936508,
        0.995608466,
        0.990740741,
        0.976243386
    };
    vector<double> y2 = {
        2285.501859,
        1540.520446,
        1005.204461,
        630.4832714,
        389.5910781,
        246.8401487
    };

    vector<double> x3 = { // 7o8, 2 WC, full sim
        0.999312169,
        0.998571429,
        0.997830688,
        0.995291005,
        0.991798942,
        0.987142857,
        0.975079365,
        0.953492063
    };
    vector<double> y3 = {
        1799.256506,
        1263.94052 ,
        853.5315985,
        559.1078067,
        353.9033457,
        215.6133829,
        130.8550186,
        77.32342007
    };

    vector<double> x4 = { // 7o8, 1 WC, full sim
        0.995185185,
        0.994338624,
        0.992962963,
        0.98968254 ,
        0.986296296,
        0.979312169,
        0.967671958,
        0.948941799
    };
    vector<double> y4 = {
        1023.048327,
        750.929368 ,
        527.8810409,
        353.9033457,
        237.9182156,
        148.6988848,
        99.62825279,
        63.94052045
    };

    vector<double> x5 = { // 6o8, 2 WC, full sim
        0.99952381 ,
        0.998783069,
        0.996349206,
        0.991587302,
        0.977089947
    };
    vector<double> y5 = {
        2138.289963,
        1335.315985,
        795.5390335,
        469.8884758,
        278.0669145
    };

    vector<vector<double>*> xs = { &x4, &x3, &x2, &x5, &x1 };
    vector<vector<double>*> ys = { &y4, &y3, &y2, &y5, &y1 };
    vector<const char *> names = { "7/8, 1WC, full-sim", "7/8, 2WC, full-sim", "6/8, 1WC, full-sim", "6/8, 2WC, full-sim", "6/8, 1WC, fast-sim"};
    vector<TGraph*> graphs;

    for (int i = 0; i < 5; i++)
    {
        TGraph *gr1 = new TGraph(xs[i]->size(), xs[i]->data(), ys[i]->data());
        gr1->SetMarkerColor(kBlack);
        gr1->SetMarkerStyle(kFullDotMedium);
        gr1->SetLineColor(kBlack);
        gr1->GetXaxis()->SetTitle("single muon efficiency");
        gr1->GetYaxis()->SetTitle("<matched patterns per event>");
        graphs.push_back(gr1);

        htt_xall.insert(htt_xall.end(), xs[i]->begin(), xs[i]->end());
        htt_yall.insert(htt_yall.end(), ys[i]->begin(), ys[i]->end());
    }

    for (double & y : htt_yall) y = 3*y;

    TCanvas *c1 = new TCanvas("c1", "canvas", 800, 800);
    c1->SetLogy();
    TColor *col = new TColor(1300, 1, 1, 1, "", 0);
    c1->SetFillColor(1300);

    TLegend l(0.18, 0.75, 0.6, 0.85);
    l.SetFillStyle(4000);
    l.SetFillColor(1300);
    l.SetLineColor(0);
    l.SetBorderSize(0);

    TGraph *gr1 = new TGraph(htt_xall.size(), htt_xall.data(), htt_yall.data());
    gr1->SetMarkerColor(kRed+1);
    gr1->SetMarkerStyle(kFullCircle);
    gr1->SetMarkerSize(1.6);
    gr1->GetXaxis()->SetTitle("efficiency");
    gr1->GetYaxis()->SetTitle("<track candidates>");
    //gr1->GetYaxis()->SetTitleOffset(1.6);
    gr1->SetTitle("");
    gr1->Draw("AP");
    //ATLASLabel(0.2, 0.85, "Simulation Internal");

    l.SetTextSize(0.04);
    l.SetTextFont(42);
    l.AddEntry(gr1, "pattern banks from", "P");
    l.AddEntry(gr1, "various configurations", "");
    l.Draw();

    TLatex *tex = new TLatex(0.22, 0.9, "#eta #in [0.1, 0.3], #phi #in [0.3, 0.5]");
    tex->SetNDC();
    tex->SetTextSize(0.04);
    tex->Draw();
    tex->SetTextAlign(13);

    c1->Print("HTT_DC-summary.pdf");
}

// x = eff, y = # combs
void hough_summary()
{
    ///////////////////////////////////////////////////////////////////////////
    // 21-04-15 presentation (overlap removal)
    // scan in local max window size
    // truth matched (matching hits in 4 layers)
    ///////////////////////////////////////////////////////////////////////////

    // 216x216, 6 slices, [2,1,0*6] extension, 7/8
    vector<double> x1 = { 99.57, 99.55, 99.54, 99.54 };
    vector<double> y1 = { 4071, 2380, 1566, 1296 };

    // 216x216, 6 slices, [2,1,0*6] extension, 7/9
    vector<double> x2 = { 99.89, 99.89, 99.87, 99.87 };
    vector<double> y2 = { 6590, 3642, 2512, 2154 };

    // 64x216, 19 slices, no extension, 7/8
    vector<double> x3 = { 99.43, 99.43, 99.41 };
    vector<double> y3 = { 3274, 2010, 1516 };

    // 64x216, 19 slices, no extension, 7/9
    vector<double> x4 = { 99.9, 99.9, 99.87 };
    vector<double> y4 = { 11325, 6201, 4517 };

    ///////////////////////////////////////////////////////////////////////////
    // 21-03-16 presentation (6o8)
    // Crude effs
    ///////////////////////////////////////////////////////////////////////////

    // Benchmarks
    vector<double> x5 = { 98.5, 98.8, 98.4 };
    vector<double> y5 = { 3128, 1892, 728 };

    // 1200 phi bins
    vector<double> x6 = { 98, 98.5 };
    vector<double> y6 = { 3766, 2116 };

    // N-2 threshold
    vector<double> x7 = { 99.71, 99.81, 99.83, 99.84, 99.86, 99.12, 99.47, 99.55, 99.65, 99.71 };
    vector<double> y7 = { 5449, 9727, 14107, 15519, 22445, 2481, 4412, 6417, 11467, 16554 };

    // Less phi bins
    vector<double> x8 = { 98.66, 98.4 };
    vector<double> y8 = { 6289, 1828 };

    // 2D alternate slicing, 216x216
    vector<double> x9 = { 98.3, 98.2, 98.2, 98, 98.2, 97.5, 96.7 };
    vector<double> y9 = { 4718, 2409, 3317, 1269, 1723, 481, 279 };

    ///////////////////////////////////////////////////////////////////////////
    // 21-02-25 (1D transform)
    // Crude effs
    ///////////////////////////////////////////////////////////////////////////

    // 100x216
    vector<double> x10 = { 98.5, 99 };
    vector<double> y10 = { 8287, 11696 };


    vector<vector<double>*> xs = { &x1, &x2, &x3, &x4, &x5, &x6, &x7, &x8, &x9, &x10 };
    vector<vector<double>*> ys = { &y1, &y2, &y3, &y4, &y5, &y6, &y7, &y8, &y9, &y10 };

    for (unsigned i = 0; i < xs.size(); i++)
    {
        hough_xall.insert(hough_xall.end(), xs[i]->begin(), xs[i]->end());
        hough_yall.insert(hough_yall.end(), ys[i]->begin(), ys[i]->end());
    }

    for (double &x : hough_xall) x = x / 100;


    TCanvas *c1 = new TCanvas("c1", "canvas", 800, 800);
    c1->SetLogy();
    TColor *col = new TColor(1300, 1, 1, 1, "", 0);
    c1->SetFillColor(1300);

    TLegend l(0.18, 0.75, 0.6, 0.85);
    l.SetFillStyle(4000);
    l.SetFillColor(1300);
    l.SetLineColor(0);
    l.SetBorderSize(0);

    TGraph *gr1 = new TGraph(hough_xall.size(), hough_xall.data(), hough_yall.data());
    gr1->SetMarkerColor(kRed+1);
    gr1->SetMarkerStyle(kFullCircle);
    gr1->SetMarkerSize(1.6);
    gr1->GetXaxis()->SetTitle("efficiency");
    gr1->GetYaxis()->SetTitle("<track candidates>");
    gr1->GetXaxis()->SetNdivisions(505, true);
    //gr1->GetYaxis()->SetTitleOffset(1.6);
    gr1->SetTitle("");
    gr1->Draw("AP");
    //ATLASLabel(0.2, 0.85, "Simulation Internal");

    l.SetTextSize(0.04);
    l.SetTextFont(42);
    l.AddEntry(gr1, "Hough transforms of", "P");
    l.AddEntry(gr1, "various configurations", "");
    l.Draw();

    TLatex *tex = new TLatex(0.22, 0.9, "#eta #in [0.1, 0.3], #phi #in [0.3, 0.5]");
    tex->SetNDC();
    tex->SetTextSize(0.04);
    tex->Draw();
    tex->SetTextAlign(13);

    c1->Print("Hough_summary.pdf");
}

void htt_vs_hough()
{
    TGraph *gr1 = new TGraph(htt_xall.size(), htt_xall.data(), htt_yall.data());
    gr1->SetMarkerColor(kBlue+1);
    gr1->SetMarkerStyle(kFullCircle);
    gr1->SetMarkerSize(1.6);
    gr1->GetXaxis()->SetTitle("efficiency");
    gr1->GetYaxis()->SetTitle("<track candidates>");
    //gr1->GetYaxis()->SetTitleOffset(1.6);
    gr1->SetTitle("");
    gr1->Draw("AP");

    TGraph *gr2 = new TGraph(hough_xall.size(), hough_xall.data(), hough_yall.data());
    gr2->SetMarkerColor(kRed+1);
    gr2->SetMarkerStyle(kFullCircle);
    gr2->SetMarkerSize(1.6);
    gr2->GetXaxis()->SetTitle("efficiency");
    gr2->GetYaxis()->SetTitle("<track candidates>");
    //gr1->GetYaxis()->SetTitleOffset(1.6);
    gr2->SetTitle("");

    TCanvas *c1 = new TCanvas("c1", "canvas", 800, 800);
    c1->SetLogy();
    TColor *col = new TColor(1300, 1, 1, 1, "", 0);
    c1->SetFillColor(1300);

    TLegend l(0.18, 0.75, 0.6, 0.85);
    l.SetFillStyle(4000);
    l.SetFillColor(1300);
    l.SetLineColor(0);
    l.SetBorderSize(0);

    TMultiGraph *mg = new TMultiGraph();
    mg->Add(gr1, "p");
    mg->Add(gr2, "p");
    mg->Draw("a");
    mg->GetXaxis()->SetTitle("efficiency");
    mg->GetYaxis()->SetTitle("<track candidates>");

    l.SetTextSize(0.04);
    l.SetTextFont(42);
    l.AddEntry(gr1, "HTT", "P");
    l.AddEntry(gr2, "Hough Transform", "P");
    l.Draw();

    TLatex *tex = new TLatex(0.22, 0.9, "#eta #in [0.1, 0.3], #phi #in [0.3, 0.5]");
    tex->SetNDC();
    tex->SetTextSize(0.04);
    tex->Draw();
    tex->SetTextAlign(13);

    c1->Print("Hough_vs_HTT.pdf");
}

void plot()
{
    vector<vector<double>> hits = {
{36.1976, 13.812 , -50.4775},
{94.1005, 31.3658, -43.1765},
{157.757, 48.5195, -35.1255},
{157.757, 48.5195, -35.1755},
{150.012, 46.5672, -36.1255},
{218.815, 63.0111, -27.4255},
{218.815, 63.0111, -27.4755},
{283.935, 76.3263, -19.2255},
{283.936, 76.3763, -19.2255},
{377.567, 93.7691, -86.3728},
{384.362, 90.7217, -84.3041},
{543.852, 108.628, 12.2276 },
{550.264, 109.023, 13.3915 },
{747.349, 111.319, 24.4801 },
{747.424, 113.129, 24.5272 },
{753.74 , 110.478, 25.3516 },
{1002.25, 86.7784, 26.0307 },
{1008.25, 83.4662, 23.8481 },
{985.711, 89.3511, 23.8304 },
{991.954, 86.2728, 26.0562 }};



    vector<double> x;
    vector<double> y;
    vector<double> z;

    for (auto h : hits)
    {
        x.push_back(h[0]);
        y.push_back(h[1]);
        z.push_back(h[2]);
    }


    TCanvas *c1 = new TCanvas("c1", "canvas", 1200, 900);
    c1->Divide(2,2);

    c1->cd(1);
    TGraph *gr1 = new TGraph(x.size(), x.data(), y.data());
    gr1->SetMarkerColor(kBlack);
    gr1->SetMarkerStyle(kFullDotMedium);
    gr1->GetXaxis()->SetTitle("x");
    gr1->GetYaxis()->SetTitle("y");
    gr1->SetTitle("y vs x");
    gr1->Draw("AP");

/*
    TGraph *gr1o = new TGraph(n, xo, yo);
    gr1o->SetMarkerColor(8);
    gr1o->SetMarkerStyle(kCircle);
    gr1o->Draw("P");
*/

    c1->cd(2);
    TGraph *gr2 = new TGraph(x.size(), z.data(), x.data());
    gr2->SetMarkerColor(kBlack);
    gr2->SetMarkerStyle(kFullDotMedium);
    gr2->GetXaxis()->SetTitle("z");
    gr2->GetYaxis()->SetTitle("x");
    gr2->SetTitle("x vs z");
    gr2->Draw("AP");

/*
    TGraph *gr2o = new TGraph(n, zo, xo);
    gr2o->SetMarkerColor(8);
    gr2o->SetMarkerStyle(kCircle);
    gr2o->Draw("P");
*/

    c1->cd(3);
    TGraph *gr3 = new TGraph(x.size(), z.data(), y.data());
    gr3->SetMarkerColor(kBlack);
    gr3->SetMarkerStyle(kFullDotMedium);
    gr3->GetXaxis()->SetTitle("z");
    gr3->GetYaxis()->SetTitle("y");
    gr3->SetTitle("y vs z");
    gr3->Draw("AP");

/*
    TGraph *gr3o = new TGraph(n, zo, yo);
    gr3o->SetMarkerColor(8);
    gr3o->SetMarkerStyle(kCircle);
    gr3o->Draw("P");
*/

    c1->Print("graph.png","png");
}

void npat_v_ntrial()
{
    int x[5];
    for (int i = 5; i >= 1; i--) x[5-i] = 2*i;

    int y[5] = {
    45786928,
    42974479,
    39363044,
    34342991,
    26156624,
    };

    TCanvas *c1 = new TCanvas("c1", "canvas", 800, 600);
    TGraph *gr = new TGraph(5, x ,y);

    gr->SetTitle("");
    gPad->SetLeftMargin(0.15);
    gr->Draw("a"); // make axes
    gr->GetYaxis()->SetTitleOffset(1.2);
    gr->GetYaxis()->SetTitle("Number of patterns");
    gr->GetXaxis()->SetTitle("Number of trials (billions of single muons)");
    gr->SetMarkerColor(kBlack);
    gr->SetMarkerStyle(kFullSquare);
    gr->Draw("ap");
    c1->Print("graph.png","png");

    delete c1;
}

void npat_eff_v_ntrial()
{
    double x[5] = {10, 8, 6, 4, 2};
    double y1[5] = {
    45786928, // #DC patterns
    42974479,
    39363044,
    34342991,
    26156624,
    };
    double y2[5] = {
    0.985405, // efficiencies
    0.984934,
    0.984227,
    0.983050,
    0.979519,
    };

    // --- set up canvas
    gROOT->ForceStyle(0);
    TCanvas *c1 = new TCanvas("c1", "canvas", 800, 600);
    TPad *pad = new TPad("pad","",0,0,1,1);
    pad->Draw();
    pad->cd();
    pad->SetRightMargin(0.1);

    // --- first graph
    TH1F *hr = c1->DrawFrame(0,25000000,12,50000000);
    hr->SetYTitle("Number of DC patterns");
    hr->SetXTitle("Number of tracks generated (billions)");
    pad->GetFrame()->SetBorderSize(12);
    TGraph *gr1 = new TGraph(5, x ,y1);
    gr1->SetMarkerColor(kBlack);
    gr1->SetMarkerStyle(21);
    gr1->Draw("LP");

    // --- create a transparent pad drawn on top of the main pad
    c1->cd();
    TPad *overlay = new TPad("overlay","",0,0,1,1);
    overlay->SetRightMargin(0.1);
    overlay->SetFillStyle(0);
    overlay->SetFillColor(0);
    overlay->SetFrameFillStyle(0);
    overlay->Draw("FA");
    overlay->cd();

    // --- second graph
    TGraph *gr2 = new TGraph(5, x, y2);
    gr2->SetMarkerColor(kRed);
    gr2->SetMarkerStyle(20);
    gr2->SetName("gr2");
    Double_t xmin = pad->GetUxmin();
    Double_t ymin = 0.97;
    Double_t xmax = pad->GetUxmax();
    Double_t ymax = 0.99;
    TH1F *hframe = overlay->DrawFrame(xmin, ymin, xmax, ymax);
    hframe->GetXaxis()->SetLabelOffset(99);
    hframe->GetYaxis()->SetTickLength(0);
    hframe->GetYaxis()->SetLabelOffset(99);
    gr2->Draw("LP");

    //Draw an axis on the right side
    int ndivisions = 5; int nsubdivs = 5; int ndiv = 100 * nsubdivs + ndivisions;
    TGaxis *axis = new TGaxis(xmax,ymin,xmax, ymax,ymin,ymax,ndiv,"+L"); // 5x5 divisions
    axis->SetLineColor(kRed);
    axis->SetLabelColor(kRed);
    axis->SetLabelFont(132); // times-medium-r-normal
    axis->SetTitle("Efficiency (tracks matched / # tracks)");
    axis->SetTitleFont(132);
    axis->SetTitleOffset(1.2);
    axis->Draw();

    TLegend* legend = new TLegend(0.55, 0.15, 0.85, 0.3);
    legend->AddEntry(gr1, "Number of DC patterns", "p");
    legend->AddEntry(gr2, "Efficiency", "p");
    legend->Draw();

    c1->Print("graph.png","png");
    delete c1;
}

void eff_v_nroad()
{
    int n = 3;
    double x[n] = {0.7528249, 0.8170904, 0.8622881};
    double y[n] = {21.781891, 29.650825, 64.572890};
    string label[n] = {"No DC bits (thin SS)", "DC bits", "No DC bits (broad SS)"};

    TCanvas *c1 = new TCanvas("c1", "canvas", 800, 600);
    TGraph *gr = new TGraph(n, x ,y);

    for (int i=0; i<n; i++) {
        TLatex* latex = new TLatex(x[i], y[i], label[i].c_str());
        latex->SetTextAlign(11+10*i); // right, center, left; bottom aligned
        latex->SetTextFont(130);
        latex->SetTextSize(0.04);
        gr->GetListOfFunctions()->Add((TObject*)latex);
    }

    gr->SetTitle("");
    gPad->SetLeftMargin(0.15);
    gr->Draw("a"); // make axes
    gr->GetYaxis()->SetTitleOffset(1.2);
    gr->GetYaxis()->SetTitle("<roads/event>");
    gr->GetXaxis()->SetTitle("Efficiency");
    gr->SetMarkerColor(kBlack);
    gr->SetMarkerStyle(kFullSquare);
    gr->Draw("ap");
    c1->Print("graph.png","png");

    delete c1;
}


int main(int argc, char* argv[])
{
    SetAtlasStyle();

    christian_DC_summary();
    hough_summary();
    htt_vs_hough();
    //plot();
    return 0;
}
