/* descript.cxx
 * Riley Xu -- Sept 22 2020
 *
 * Prints the description from a pattern bank
 */

#include "TFile.h"
#include "TTree.h"
#include <iostream>

using namespace std;


int main(int argc, char* argv[])
{
    if (argc != 2) return 1;
    TFile f(argv[1]);

    string x;
    string *y = &x;
    TTree *t = (TTree*)f.Get("Metadata");
    t->SetBranchAddress("description", &y);
    t->GetEntry(0);

    cout << x << endl;

    return 0;
}
