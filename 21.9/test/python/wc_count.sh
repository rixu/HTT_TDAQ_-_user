#!/bin/bash

grep $1 -e '-1' -no | uniq -c | grep '^[[:space:]]*1' -o | wc -l
grep $1 -e '-1' -no | uniq -c | grep '^[[:space:]]*2' -o | wc -l
