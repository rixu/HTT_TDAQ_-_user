import ROOT
from array import array

#n=5
#x = array('d', [1, 2, 5, 7, 10])
#y = array('d', [946244, 1801722, 3991051, 5231617, 6871772])

arr = [
[406.764, 0.433986],
[561.911, 0.417932],
[567.521, 0.410517],
[758.921, 0.393951],
[765.52 , 0.394158],
[992.851, 0.371438],
[999.107, 0.369925],
]
r = array('f', [a[0] for a in arr])
phi = array('f', [a[1] for a in arr])

g = ROOT.TGraph(len(arr), phi, r)
g.SetTitle("r vs phi")
g.SetMarkerColor(4)
g.SetMarkerStyle(21)

c = ROOT.TCanvas("c1","c1",800,600)
c.cd()
g.Draw("AP")

#g.GetXaxis().SetTitle("Number of trials (/1 million)")
#g.GetYaxis().SetTitle("Number of unique patterns")
c.Print("plot.png")
