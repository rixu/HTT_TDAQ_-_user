import sys
sys.argv.append('-b') # Batch mode
import ROOT
ROOT.gROOT.SetBatch(ROOT.kTRUE)
import ctypes
import numpy as np


'''
fileNames = [(name, color, legend)]
'''
def Plot(fileNames, histdir, histname, opts="", logx=False, **kwargs):
    c1 = ROOT.TCanvas("c1","c1",800,600)
    print("plotting " + histname)

    files = []
    for fileName in fileNames:
        f = ROOT.TFile(fileName[0], "READ")
        print("opened file: " + fileName[0])
        files.append(f)

    if len(fileNames) > 1:
        ROOT.gStyle.SetOptStat(00000000)
        leg = ROOT.TLegend(0.1,0.7,0.48,0.9)
        leg.SetFillColor(0)
        leg.SetLineColor(0)
    else:
        ROOT.gStyle.SetOptStat(111111) # Overflow, underflow (https://root.cern.ch/doc/master/classTPaveStats.html)
    ROOT.gROOT.ForceStyle()

    c1.SetLogx(logx)

    hists = []
    for i in range(len(files)):

        hist = files[i].Get(histdir + "/" + histname)
        hist.SetLineColor(fileNames[i][1])
        hist.SetFillColor(fileNames[i][1])

        if 'x_range' in kwargs:
            hist.GetXaxis().SetRangeUser(*kwargs['x_range'])
        if 'y_range' in kwargs:
            hist.GetYaxis().SetRangeUser(*kwargs['y_range'])

        opt = opts
        if "Eff" in histname or "eff" in histname:
            opt += "P" # Points
            opt += "E" # Error bars
            #opt += "L" # Line through points
            if (i == 0): opt += "A"

        if (i != 0):
            opt = "same " + opt

        hist.Draw(opt)
        hists.append(hist)
        if len(fileNames) > 1:
            leg.AddEntry(hist, hist.GetName(), "l")

    if len(fileNames) > 1:
        leg.Draw()

    c1.Print(histname + ".png")


'''
histnames = [(name, color, legend)]
'''
def PlotStack(filename, histdir, histnames, violin="", **kwargs):
    c1 = ROOT.TCanvas("c1","c1",800,600)
    ROOT.gStyle.SetOptStat(111111) # Overflow, underflow (https://root.cern.ch/doc/master/classTPaveStats.html)
    ROOT.gStyle.SetOptTitle(0)
    ROOT.gROOT.ForceStyle();

    f = ROOT.TFile(filename, "READ")

    leg = ROOT.TLegend(0.12,0.8,0.3,0.89)
    leg.SetFillColor(0)
    leg.SetLineColor(0)

    # Get hists and means
    hists = []
    means = [ [] for _ in range(len(histnames)) ] # x, y
    for i_hist in range(len(histnames)):
        print("Reading " + histnames[i_hist][0])
        hist = f.Get(histdir + "/" + histnames[i_hist][0])
        hist.SetFillColor(histnames[i_hist][1]) # for legend
        hist.SetLineColor(histnames[i_hist][1])

        # Get means, pre-adding
        for i in range(1, hist.GetNbinsY() + 1):
            hproj = hist.ProjectionX("_py", i, i)
            if not hproj.GetEntries():
                continue

            binPosY = hist.GetYaxis().GetBinLowEdge(i)
            binWidthY = hist.GetYaxis().GetBinWidth(i)
            binPosY += binWidthY / 2.0
            means[i_hist].append((hproj.GetMean(), binPosY))

        if i_hist > 0:
            hist.Add(hists[-1])
        hists.append(hist)
        leg.AddEntry(hist, histnames[i_hist][2], "f")

    # Get maxes
    maxes = [0] * (hists[-1].GetNbinsY() + 1)
    for i_hist in range(len(histnames)):
        for i in range(1, hist.GetNbinsY() + 1):
            hproj = hists[i_hist].ProjectionX("_py", i, i)
            val = hproj.GetMaximum()
            if val > maxes[i]:
                maxes[i] = val

    # Draw histos
    #hists[-1].SetStats(0);
    hists[-1].Draw("AXIS")
    boxes = [] # So python doesn't garbage collect
    for i_hist in range(len(hists)-1, -1, -1):
        hist = hists[i_hist]

        for i in range(1, hist.GetNbinsY() + 1):
            binPosY = hist.GetYaxis().GetBinLowEdge(i)
            binWidthY = hist.GetYaxis().GetBinWidth(i)
            binPosY += binWidthY / 2.0

            for j in range(1, hist.GetNbinsX() + 1):
                binPosX = hist.GetXaxis().GetBinLowEdge(j)
                binWidthX = hist.GetXaxis().GetBinWidth(j)

                val = hist.GetBinContent(j, i)
                if val == 0:
                    continue

                height = binWidthY * 0.95 * val / maxes[i] / 2.0
                Box = ROOT.TBox(binPosX, binPosY - height, binPosX + binWidthX, binPosY + height)
                Box.SetFillColor(histnames[i_hist][1])
                Box.SetLineColor(histnames[i_hist][1])
                Box.Draw()
                boxes.append(Box)

    # Draw means
    markers = [] # So python doesn't garbage collect
    for i_hist in range(len(histnames)):
        for pos in means[i_hist]:
            marker = ROOT.TMarker(pos[0], pos[1], ROOT.kFullCircle)
            marker.SetMarkerSize(0.5)
            marker.SetMarkerColor(ROOT.TColor.GetColorDark(histnames[i_hist][1]))
            marker.Draw()
            markers.append(marker)

            marker = ROOT.TMarker(pos[0], pos[1], ROOT.kOpenCircle)
            marker.SetMarkerSize(0.5)
            marker.Draw()
            markers.append(marker)

    #hists[-1].Draw("AXIS SAMES") # Put stats box on top of z-order
    leg.Draw()
    c1.Print(kwargs.get('output', histnames[0][0] + ".png"))


def Print(filename, histdir, histname):
    print("Opening " + filename)
    f = ROOT.TFile(filename, "READ")
    h = f.Get(histdir + "/" + histname)

    if h.InheritsFrom("TH2"):
        p = h.ProfileX("_pfx", 1, -1, "s")
        latex = ''
        print("Total Entries:" + str(h.GetEntries()))
        for i in range(p.GetNbinsX()):
            print("\tBin=" + str(i+1) + ", Entries=" + str(p.GetBinEntries(i+1)) + ", Mean=" + str(p.GetBinContent(i+1)) + ", RMS=" + str(p.GetBinError(i+1)))
            latex += ' & ' + str(int(round(p.GetBinContent(i+1))))
        print(latex)


files=[
        #('rundir/test_216_6_0_/loghits.root', '_r0'),
        #('rundir/r1_216_6_0_/loghits.root', '_r1'),
        #('rundir/r4_test_0_/loghits.root', '_r4'),
        #('rundir/fixedpt_216_6_0_3/loghits.root', '_r1_1gev'),
        #('rundir/fixedpt_test_0_9/loghits.root', '_r4_1gev'),
]

for f in files:
    PlotStack(f[0], "Hough", [
            ["h_res_hitPhi_neg", 46, "q < 0"],
            ["h_res_hitPhi_pos", 30, "q > 0"],
        ], violin="VIOLINY", output='phires' + f[1] + '.png')
    PlotStack(f[0], "Hough", [
            ["h_res_hitPhi_cor_neg", 46, "q < 0"],
            ["h_res_hitPhi_cor_pos", 30, "q > 0"],
        ], violin="VIOLINY", output='phires_corr' + f[1] + '.png')


files=[
    #('rundir/fixedpt_test_0_3/loghits.root', ROOT.kGray, ''),
    #('rundir/r3_test_0_/loghits.root', ROOT.kGray, ''),
]


#Plot(files, "Hough", "h_res_hitA_neg", opts="candle")
#Plot(files, "Hough", "h_res_hitA_pos", opts="candle")
#Plot(files, "Hough", "h_res_hitPhi_qpt")

#Plot(files, "CrudeEff/EffHist/RoadEff", "h_RoadEfficiency_pt", logx=True)
#Plot(files, "CrudeEff/EffHist/RoadEff", "h_RoadEfficiency_eta")
#Plot(files, "CrudeEff/EffHist/RoadEff", "h_RoadEfficiency_phi")
#Plot(files, "CrudeEff/EffHist/RoadEff", "h_RoadEfficiency_d0")
#Plot(files, "CrudeEff/EffHist/RoadEff", "h_RoadEfficiency_z0")

#Plot(files, 'TruthMatchHist/TrackHist/Offline_vs_Truth/EffHist', 'h_Offline_vs_Truth_eff_pt', logx=True)

#Plot(files, "HTTRoadHist/AllRoadHist", "h_nRoads", x_range=(0, 50))
#Plot(files, "HTTRoadHist/AllRoadHist", "h_nRoads_matched")
#Plot(files, "HTTRoadHist/AllRoadHist", "h_road_highestBarcodeFrac")

Print('rundir/test_216_6_1_/loghits.root', 'HTTClusterHist/PerLayerHist', 'h_nClusters_layer')

