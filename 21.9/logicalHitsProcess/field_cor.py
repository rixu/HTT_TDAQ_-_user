import sys
sys.argv.append('-b') # Batch mode
import ROOT
ROOT.gROOT.SetBatch(ROOT.kTRUE)
import numpy as np


def corrections(filename):
    f = ROOT.TFile(filename, "READ")
    h = f.Get("Hough/h_res_hitPhi_qpt")

    means = [] # store (r (meters), delta phi / qpt (GeV))

    for i in range(1, h.GetNbinsY() + 1):
        hproj = h.ProjectionX("_py", i, i)
        if not hproj.GetEntries():
            continue

        binPosY = h.GetYaxis().GetBinLowEdge(i)
        binWidthY = h.GetYaxis().GetBinWidth(i)
        binPosY += binWidthY / 2.0
        means.append((binPosY / 1000, hproj.GetMean()))

    print(np.array(means))


if __name__ == "__main__":
    corrections(sys.argv[1])
