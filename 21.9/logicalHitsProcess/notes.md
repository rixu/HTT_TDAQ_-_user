Test file: `user.jahreda.21868108.EXT0._001976.httsim_rawhits_wrap.root`
| Configuration | <# roads> | Efficiency | Events |
| :--- | :--- | :--- | :--- |
| Truth 13 * 800k (21868108), jahreda\_20-07-20, max 4 [21111122] | 12.1398 | 0.993711 | 20k |
| Truth 13 * 800k (21868108), jahreda\_20-07-20, max 4 [21111122] | 12.1604 | 0.995646 | 2k |
| Truth 13 * 800k (21868108), jahreda\_20-07-20, max 4 [10000011], 3.75M | 10.5334 | 0.989115 | 2k |
| Truth 13 * 800k (21868108), jahreda\_20-07-20, noDC, 3.75M | 8.60305 | 0.959361 | 2k |
| Truth 13 * 800k (21868108), jahreda\_20-07-20, noDC | 12.7467 | 0.98984 | 2k |

