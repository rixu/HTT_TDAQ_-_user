#!/bin/bash

if [ -z ${1+x} ]; then
    setupATLAS
    asetup Athena,21.9.16,testarea=$PWD/build
    source build/x86_64-*-gcc62-opt/setup.sh
    echo "Setup all"
elif [ "$1" = "build" ]; then
    source build/x86_64-*-gcc62-opt/setup.sh
else
    echo "unkown: $1"
fi
