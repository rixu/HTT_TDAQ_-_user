#!/bin/bash

set -e

cd build
#ctest -R 'LogicalHits|OutputMon' -j5

mv Trigger/TrigHTT/TrigHTTAlgorithms/unitTestRun_testLogicalHitsToAlg_0/testLogicalHitsToAlg_0.log-todiff /afs/cern.ch/work/r/rixu/private/HTT_TDAQ/athena/Trigger/TrigHTT/TrigHTTAlgorithms/share/testLogicalHitsToAlg_0.ref
mv Trigger/TrigHTT/TrigHTTAlgorithms/unitTestRun_testLogicalHitsToAlg_1/testLogicalHitsToAlg_1.log-todiff /afs/cern.ch/work/r/rixu/private/HTT_TDAQ/athena/Trigger/TrigHTT/TrigHTTAlgorithms/share/testLogicalHitsToAlg_1.ref
mv Trigger/TrigHTT/TrigHTTAlgorithms/unitTestRun_testLogicalHitsToAlg_Hough/testLogicalHitsToAlg_Hough.log-todiff /afs/cern.ch/work/r/rixu/private/HTT_TDAQ/athena/Trigger/TrigHTT/TrigHTTAlgorithms/share/testLogicalHitsToAlg_Hough.ref
mv Trigger/TrigHTT/TrigHTTAlgorithms/unitTestRun_testOutputMonitorAlg/testOutputMonitorAlg.log-todiff /afs/cern.ch/work/r/rixu/private/HTT_TDAQ/athena/Trigger/TrigHTT/TrigHTTAlgorithms/share/testOutputMonitorAlg.ref

# mv Trigger/TrigHTT/TrigHTTInput/unitTestRun_testDumpOutputStat/testDumpOutputStat.log-todiff /afs/cern.ch/work/r/rixu/private/HTT_TDAQ/athena/Trigger/TrigHTT/TrigHTTInput/share/testDumpOutputStat.ref
# mv Trigger/TrigHTT/TrigHTTByteStream/unitTestRun_testHTTByteStreamConversionDummy/testHTTByteStreamConversionDummy.log-todiff /afs/cern.ch/work/r/rixu/private/HTT_TDAQ/athena/Trigger/TrigHTT/TrigHTTByteStream/share/testHTTByteStreamConversionDummy.ref
