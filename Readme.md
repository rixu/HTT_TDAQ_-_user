#### Workflow
-------------------------------------------------

    $ source setup.sh
    $ (edit code)
    $ make [cmake]
    $ source setup.sh build
    $ cd build; ctest [-j NJOBS] [-R regexp]

#### Journal
-------------------------------------------------

* 7/5/19 Recompiled everything in 21.9. Note that switching releases requires completely recompiling
(i.e. rm -rf build/).

#### References
-------------------------------------------------
* [Twiki](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/HTTSimCoreDev)
* [HTT git repo](https://gitlab.cern.ch/atlas-tdaq-ph2upgrades/atlas-tdaq-htt/tdaq-htt-offline/athena/tree/21.0/Trigger/TrigHTT)
* [This test repo](https://gitlab.cern.ch/rixu/HTT_TDAQ_-_user)
