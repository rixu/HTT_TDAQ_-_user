default:
	$(MAKE) -C build/ --no-print-directory

cmake:
	cd build; cmake ../athena/Projects/TrigHTT;
